#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->showMaximized();

    CPs = new ControlPoints(this);
    route = new Route(this);

    connect(this, &MainWindow::imageSet, ui->widget_map, &MapWidget::loadImage);
    connect(this, &MainWindow::mapModeChanged, ui->widget_map, &MapWidget::setMapMode);

    connect(ui->widget_map, &MapWidget::scaleSet, this, &MainWindow::setMapScale);
    connect(this, &MainWindow::mapScaleSet, route, &Route::setMapScale);
    connect(this, &MainWindow::mapScaleSet, CPs, &ControlPoints::setMapScale);

    connect(ui->widget_map, &MapWidget::CPAdded, this, &MainWindow::setPointParameters);
    connect(this, &MainWindow::pointAdded, CPs, &ControlPoints::addCP);
    connect(this, &MainWindow::pointRefused, ui->widget_map, &MapWidget::removeLastPoint);
    connect(CPs, &ControlPoints::controlPointsChanged, this, &MainWindow::fillCPTable);

    connect(ui->widget_map, &MapWidget::routePointAdded, this, &MainWindow::setRoutePointParameters);
    connect(this, &MainWindow::addNamedPointToRoute, route, &Route::addNamedRoutePoint);
    connect(route, &Route::routeChanged, this, &MainWindow::fillRouteTable);
    connect(route, &Route::pathChanged, this, &MainWindow::fillPath);
    connect(route, &Route::routeScoreChanged, this, &MainWindow::fillRouteScore);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionOpen_Map_Image_triggered()
{
    QString imageFileName = QFileDialog::getOpenFileName(this, tr("Open Map Image"), QApplication::applicationDirPath()/*, tr("Image files (*.jpg, *.JPG, *.JPEG, *.png, *.PNG")*/);
    if (!imageFileName.isEmpty())
    {
        emit imageSet(imageFileName);
    }
}

void MainWindow::fillCPTable(QVector<QString> cpNames, QVector<int> cpScores, QVector<QPointF> cpPositions)
{
    ui->tableWidget_controlPoints->clear();
    ui->tableWidget_controlPoints->setColumnCount(4);
    ui->tableWidget_controlPoints->setHorizontalHeaderItem(MainWindow::cpName, new QTableWidgetItem(tr("Имя")));
    ui->tableWidget_controlPoints->setHorizontalHeaderItem(MainWindow::cpScore, new QTableWidgetItem(tr("Очки")));
    ui->tableWidget_controlPoints->setHorizontalHeaderItem(MainWindow::cpX, new QTableWidgetItem(tr("X")));
    ui->tableWidget_controlPoints->setHorizontalHeaderItem(MainWindow::cpY, new QTableWidgetItem(tr("Y")));
    ui->tableWidget_controlPoints->setRowCount(cpNames.size());
    for(int i=0; i<cpNames.size(); ++i)
    {
        ui->tableWidget_controlPoints->setItem(i, MainWindow::cpName, new QTableWidgetItem(cpNames.at(i)));
        ui->tableWidget_controlPoints->setItem(i, MainWindow::cpScore, new QTableWidgetItem(QString::number(cpScores.at(i))));
        ui->tableWidget_controlPoints->setItem(i, MainWindow::cpX, new QTableWidgetItem(QString::number(cpPositions.at(i).x())));
        ui->tableWidget_controlPoints->setItem(i, MainWindow::cpY, new QTableWidgetItem(QString::number(cpPositions.at(i).y())));
    }
}

void MainWindow::fillRouteTable(QVector<QPointF> route, QVector<QString> routePointNames)
{
    ui->tableWidget_route->clear();
    ui->tableWidget_route->setColumnCount(3);
    ui->tableWidget_route->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Имя")));
    ui->tableWidget_route->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("X")));
    ui->tableWidget_route->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Y")));
    ui->tableWidget_route->setRowCount(route.size());
    for(int i=0; i<route.size(); ++i)
    {
        ui->tableWidget_route->setItem(i, 0, new QTableWidgetItem(routePointNames.at(i)));
        ui->tableWidget_route->setItem(i, 1, new QTableWidgetItem(QString::number(route.at(i).x())));
        ui->tableWidget_route->setItem(i, 2, new QTableWidgetItem(QString::number(route.at(i).y())));
    }
}

void MainWindow::fillPath(double path)
{
    ui->doubleSpinBox_path->setValue(path);
}

void MainWindow::fillRouteScore(int score)
{
    ui->spinBox_routeScore->setValue(score);
}

void MainWindow::setPointParameters(QPoint cp)
{
    QString pointName;
    int pointScore;
    SetCPDialog dlg(&pointName, &pointScore, this);
    if (dlg.exec() == QDialog::Accepted)
    {
        emit pointAdded (pointName, pointScore, cp);
    }
    else
    {
        emit pointRefused();
    }
}

void MainWindow::setRoutePointParameters(QPoint routePoint, int cpMatchedIndex)
{
    QString name = CPs->getCPName(cpMatchedIndex);
    int score = CPs->getCPScore(cpMatchedIndex);
    emit addNamedPointToRoute(name, score, routePoint);
}

void MainWindow::setMapScale(QLine scaleLine)
{
    double lineLength = sqrt( pow(scaleLine.p1().x() - scaleLine.p2().x(), 2) + pow(scaleLine.p1().y() - scaleLine.p2().y(), 2) );
    double mapDistance = QInputDialog::getDouble(this, tr("Расстояние по карте"), tr("Введите расстояние по карте в км"));
    double scale = mapDistance / lineLength;
    emit mapScaleSet(scale);
}

void MainWindow::on_action_setScale_triggered(bool checked)
{
    if (checked)
    {
        ui->action_addPoint->setChecked(!checked);
        ui->action_leadRoute->setChecked(!checked);
        emit mapModeChanged(MapWidget::scaleSetting);
    }
    else
    {
        emit mapModeChanged(MapWidget::surfing);
    }
}

void MainWindow::on_action_addPoint_triggered(bool checked)
{
    if (checked)
    {
        ui->action_leadRoute->setChecked(!checked);
        ui->action_setScale->setChecked(!checked);
        emit mapModeChanged(MapWidget::cpAddition);
    }
    else
    {
        emit mapModeChanged(MapWidget::surfing);
    }
}

void MainWindow::on_action_leadRoute_triggered(bool checked)
{
    if (checked)
    {
        ui->action_addPoint->setChecked(!checked);
        ui->action_setScale->setChecked(!checked);
        emit mapModeChanged(MapWidget::routeLeading);
    }
    else
    {
        emit mapModeChanged(MapWidget::surfing);
    }
}

void MainWindow::on_doubleSpinBox_meanSpeed_valueChanged(double arg1)
{
    ui->doubleSpinBox_pathLimit->setValue(arg1 * (double)ui->spinBox_timeLimit->value());
}

void MainWindow::on_spinBox_timeLimit_valueChanged(int arg1)
{
    ui->doubleSpinBox_pathLimit->setValue(arg1 * ui->doubleSpinBox_meanSpeed->value());
}
