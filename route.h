#ifndef ROUTE_H
#define ROUTE_H

#include <QObject>
#include <QVector>
#include <QPoint>
#include<cmath>

class Route : public QObject
{
    Q_OBJECT
public:
    explicit Route(QObject *parent = nullptr);

    double path();
    int routeScore();


signals:

    void pathChanged (double newPath);
    void routeScoreChanged(int newScore);
    void routeChanged (QVector<QPointF> route,  QVector <QString> routePointNames);


public slots:

    void addNamedRoutePoint (QString name, int score, QPoint point);
    void setMapScale (double scale);

private:

    QVector <QPointF> routeLine;
    QVector <int> cpContainedScore;
    QVector <QString> routePointNames;
    double kmPerPx = 0.0;


};

#endif // ROUTE_H
