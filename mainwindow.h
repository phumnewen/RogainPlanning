#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QInputDialog>
#include "mapwidget.h"
#include "setcpdialog.h"
#include "controlpoints.h"
#include "route.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT


public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    enum cpTableColumns
    {
        cpName = 0,
        cpScore = 1,
        cpX = 2,
        cpY = 3
    };


signals:

    void imageSet (QString imageFileName);
    void mapModeChanged (MapWidget::MapMode mode);

    void mapScaleSet(double kmPerPixel);

    void pointAdded (QString name, int score, QPoint position);
    void pointRefused();

    void addNamedPointToRoute(QString name, int score, QPoint position);


private slots:
    void setPointParameters (QPoint cp);
    void setRoutePointParameters (QPoint routePoint, int cpMatchedIndex);
    void setMapScale(QLine scaleLine);

    void fillCPTable(QVector <QString> cpNames, QVector <int> cpScores, QVector<QPointF> cpPositions);
    void fillRouteTable(QVector<QPointF> route, QVector<QString> routePointNames);
    void fillPath (double path);
    void fillRouteScore (int score);

    void on_actionOpen_Map_Image_triggered();

    void on_action_setScale_triggered(bool checked);

    void on_action_addPoint_triggered(bool checked);

    void on_action_leadRoute_triggered(bool checked);

    void on_doubleSpinBox_meanSpeed_valueChanged(double arg1);

    void on_spinBox_timeLimit_valueChanged(int arg1);


private:

    Ui::MainWindow *ui;

    ControlPoints *CPs;
    Route *route;


};

#endif // MAINWINDOW_H
