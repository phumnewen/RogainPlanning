#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QObject>
#include <QVector>

class calculator : public QObject
{
    Q_OBJECT
public:
    explicit calculator(QObject *parent = nullptr);

    QVector<int> findPath(int minScore, float maxDist);

signals:

public slots:

private:

    // Input parameters
    int timeLimit;      // hours
    float meanSpeed;    // km/h

    QVector<int> edges;
    QVector<QPair<int,int>> edgeEnds;
    QVector<int> nodeWeights;


    float dist (int time, float speed);


};

#endif // CALCULATOR_H
