#include "controlpoints.h"

ControlPoints::ControlPoints(QObject *parent) : QObject(parent)
{

}

void ControlPoints::addCP(QString name, int score, QPoint position)
{
    // Масштабируем по масштабу карты
    QPointF positionF;
    positionF.setX(position.x() * kmPerPx);
    positionF.setY(position.y() * kmPerPx);

    cpNames.push_back(name);
    cpScores.push_back(score);
    cpPositions.push_back(positionF);

    emit controlPointsChanged(cpNames, cpScores, cpPositions);
}

void ControlPoints::removeCP(int CPIndex)
{
    if (CPIndex >= 0 && CPIndex < cpNames.size())
    {
        cpNames.remove(CPIndex);
        cpScores.remove(CPIndex);
        cpPositions.remove(CPIndex);

        emit controlPointsChanged(cpNames, cpScores, cpPositions);
    }
}

void ControlPoints::renameCP(int CPIndex, QString newName)
{
    if (CPIndex >= 0 && CPIndex < cpNames.size())
    {
        cpNames.replace(CPIndex, newName);

        emit controlPointsChanged(cpNames, cpScores, cpPositions);
    }
}

void ControlPoints::setCPScore(int CPIndex, int newScore)
{
    if (CPIndex >= 0 && CPIndex < cpScores.size())
    {
        cpScores.replace(CPIndex, newScore);

        emit controlPointsChanged(cpNames, cpScores, cpPositions);
    }
}

void ControlPoints::changeCPPosition(int CPIndex, QPoint newPosition)
{
    // Масштабируем по масштабу карты
    QPointF newPositionF;
    newPositionF.setX(newPosition.x() * kmPerPx);
    newPositionF.setY(newPosition.y() * kmPerPx);

    if (CPIndex >= 0 && CPIndex < cpPositions.size())
    {
        cpPositions.replace(CPIndex, newPositionF);

        emit controlPointsChanged(cpNames, cpScores, cpPositions);
    }
}

QString ControlPoints::getCPName(int CPIndex)
{
    if (CPIndex >= 0 && CPIndex < cpNames.size())
    {
        return cpNames.at(CPIndex);
    }
    return QString();
}

int ControlPoints::getCPScore(int CPIndex)
{
    if (CPIndex >= 0 && CPIndex < cpScores.size())
    {
        return cpScores.at(CPIndex);
    }
    return 0;
}

void ControlPoints::setMapScale(double scale)
{
    kmPerPx = scale;
}
