#include "route.h"

Route::Route(QObject *parent) : QObject(parent)
{

}

double Route::path()
{
    double length = 0;
    for (int i=1; i<routeLine.size(); ++i)
    {
        QPointF prev = routeLine.at(i-1);
        QPointF current = routeLine.at(i);
        length += sqrt(pow(current.x() - prev.x(), 2) + pow(current.y() - prev.y(), 2));
    }
    return length;
}

int Route::routeScore()
{
    int score = 0;
    for (int cpScore: cpContainedScore)
    {
        score +=cpScore;
    }
    return score;
}

void Route::addNamedRoutePoint(QString name, int score, QPoint point)
{
    // Масштабируем по масштабу карты
    QPointF pointF;
    pointF.setX(point.x() * kmPerPx);
    pointF.setY(point.y() * kmPerPx);

    routeLine.push_back(pointF);
    routePointNames.push_back(name);
    cpContainedScore.push_back(score);
    emit routeChanged(routeLine, routePointNames);
    emit pathChanged(path());
    emit routeScoreChanged(routeScore());
}

void Route::setMapScale(double scale)
{
    kmPerPx = scale;
}
