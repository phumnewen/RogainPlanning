#include "mapwidget.h"

MapWidget::MapWidget(QWidget *parent) : QWidget(parent)
{
    mapMode = MapWidget::surfing;
    pointCircleDiam = 20;
    pointDotDiam = 5;
}

void MapWidget::loadImage(QString imageFileName)
{
    map = QImage(imageFileName);
    mapVisibleArea = QRect(0, 0, map.width(), map.height());
    repaint();
}

void MapWidget::setMapMode(MapWidget::MapMode mode)
{
    mapMode = mode;
}

void MapWidget::removeLastPoint()
{
    CPs.remove(CPs.size()-1);
}

void MapWidget::paintEvent(QPaintEvent *event)
{
    if (map.isNull() || mapVisibleArea.isNull() || mapVisibleArea.isEmpty())
    {
        return;
    }
    QPainter mypainter(this);
    mypainter.setRenderHint(QPainter::Antialiasing, true);

    // Map drawing
    int mapScreenWidth = height() * mapVisibleArea.width() / mapVisibleArea.height();
    QRect paintRect ((width() - mapScreenWidth) / 2, 0, mapScreenWidth, height());
    mypainter.drawImage(paintRect, map, mapVisibleArea);

    // Scale line Drawing (in scale setting mode only)
    if (mapMode == MapWidget::scaleSetting && !scaleLine.isNull())
    {
        mypainter.setPen(QPen(Qt::blue, Qt::SolidLine));
        mypainter.setBrush(Qt::NoBrush);
        QLine line;
        line.setP1(QPoint(mapToWidgetPos(scaleLine.p1())));
        line.setP2(QPoint(mapToWidgetPos(scaleLine.p2())));
        mypainter.drawLine(scaleLine);
    }

    // Control Points drawing
    for (QPoint point: CPs)
    {
        point = mapToWidgetPos(point);
        mypainter.setPen(QPen(Qt::red, Qt::SolidLine));
        mypainter.setBrush(Qt::NoBrush);
        mypainter.drawEllipse(point.x()-pointCircleDiam/2, point.y()-pointCircleDiam/2, pointCircleDiam, pointCircleDiam);
        mypainter.setBrush(QBrush(Qt::red, Qt::SolidPattern));
        mypainter.drawEllipse(point.x()-pointDotDiam/2, point.y()-pointDotDiam/2, pointDotDiam, pointDotDiam);
    }

    // Route temporarly line drawing
    if (this->hasMouseTracking() && !routePoints.isEmpty())
    {
        mypainter.setPen(QPen(Qt::green, Qt::SolidLine));
        mypainter.drawLine(mapToWidgetPos(routePoints.last()), cursor().pos());
    }

    // Route line drawing
    // route vector in widget coordinates
    QPolygon route;
    for (QPoint point: routePoints)
    {
        route.push_back(mapToWidgetPos(point));
    }

    mypainter.setPen(QPen(Qt::green, 3, Qt::SolidLine));
    mypainter.setBrush(Qt::NoBrush);
    mypainter.drawPolyline(route);

    event->accept();
}

void MapWidget::mousePressEvent(QMouseEvent *event)
{
    // Если карта находится в режиме проведения пути, то при нажатой ЛКМ будет рисоваться линия из последней точки к курсору мыши
    if (mapMode == MapWidget::scaleSetting)
    {
        scaleLine.setP1(widgetToMapPos(event->pos()));
        this->setMouseTracking(true);
    }
    if (mapMode == MapWidget::routeLeading)
    {
        this->setMouseTracking(true);
    }
    if (mapMode == MapWidget::surfing)
    {
        this->setMouseTracking(true);
        dragStartPoint = event->pos();
        cursor().setShape(Qt::ClosedHandCursor);
    }
    event->accept();
}

void MapWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (map.isNull())
    {
        return;
    }
    if (mapMode == MapWidget::scaleSetting)
    {
        setMouseTracking(false);
        scaleLine.setP2(widgetToMapPos(event->pos()));
        scaleIsSet = true;
        emit scaleSet(scaleLine);
    }
    if (mapMode == MapWidget::surfing)
    {
        this->setMouseTracking(false);
        cursor().setShape(Qt::OpenHandCursor);
    }
    if (mapMode == MapWidget::cpAddition)
    {
        if (!scaleIsSet)
        {
            QMessageBox::critical(this, tr("Ошибка добавления точки"), tr("Не задан масштаб карты"));
            return;
        }
        QPoint cp = widgetToMapPos(event->pos());
        CPs.push_back(cp);
        emit CPAdded(cp);
    }
    else if (mapMode == MapWidget::routeLeading)
    {
        if (!scaleIsSet)
        {
            QMessageBox::critical(this, tr("Ошибка добавления точки"), tr("Не задан масштаб карты"));
            return;
        }
        setMouseTracking(false);
        QPoint routePoint = widgetToMapPos(event->pos());
        int cpMatchedIndex = -1;
        // Если точка маршрута близко от контрольного пуункта, считается, что точка маршрута попала в КП
        for (QPoint cp: CPs)
        {
            if (fabs(routePoint.x() - cp.x()) < pointCircleDiam/2
                    &&
                    fabs(routePoint.y() - cp.y()) < pointCircleDiam/2)
            {
                routePoint = cp;
                cpMatchedIndex = CPs.indexOf(cp);
                break;
            }
        }
        routePoints.push_back(routePoint);
        emit routePointAdded(routePoint, cpMatchedIndex);
    }
    repaint();
    event->accept();
}

void MapWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (this->hasMouseTracking())
    {
        if (mapMode == MapWidget::surfing)
        {
            int dx = event->pos().x() - dragStartPoint.x();
            int dy = event->pos().y() - dragStartPoint.y();
            mapVisibleArea.setTopLeft(QPoint(mapVisibleArea.left() + dx, mapVisibleArea.top() + dy));
            dragStartPoint = event->pos();
        }
        repaint();
    }
}

void MapWidget::wheelEvent(QWheelEvent *event)
{
    if (map.isNull() || mapVisibleArea.isNull() || mapVisibleArea.isEmpty())
    {
        return;
    }
    float oneWheelDeltaScaleRation = 0.01;
    int delta = event->delta();

    if (delta < 0 && mapVisibleArea.height() <= height())
    {
        return;
    }

    int newImWidth = mapVisibleArea.width() * (1 - oneWheelDeltaScaleRation * delta);
    if (height() * mapVisibleArea.width() / mapVisibleArea.height() <= width())
    {
        newImWidth = mapVisibleArea.width();
    }
    int newImHeight = mapVisibleArea.height() * (1 - oneWheelDeltaScaleRation * delta);

    int imPosX = event->pos().x() * mapVisibleArea.width() / width() + mapVisibleArea.left();
    int imPosY = event->pos().y() * mapVisibleArea.height() / height() + mapVisibleArea.top();

    int newImLeft = imPosX - event->pos().x() * newImWidth / width();
    int newImTop = imPosY - event->pos().y() * newImHeight / height();

    mapVisibleArea = QRect(newImLeft, newImTop, newImWidth, newImHeight);

    repaint();
    event->accept();
}

void MapWidget::enterEvent(QEnterEvent *event)
{
    Q_UNUSED(event);
    if (mapMode == MapWidget::surfing)
    {
        cursor().setShape(Qt::OpenHandCursor);
    }
    else if (mapMode == MapWidget::scaleSetting)
    {
        cursor().setShape(Qt::SizeHorCursor);
    }
    else if (mapMode ==  MapWidget::cpAddition)
    {
        cursor().setShape(Qt::CrossCursor);
    }
    else if (mapMode ==  MapWidget::routeLeading)
    {
        cursor().setShape(Qt::PointingHandCursor);
    }
}

void MapWidget::leaveEvent(QEvent *event)
{
    Q_UNUSED(event);
    cursor().setShape(Qt::ArrowCursor);
}

QPoint MapWidget::widgetToMapPos(QPoint widgetPos)
{
    if (mapVisibleArea.isNull() || mapVisibleArea.isEmpty())
    {
        return QPoint (0, 0);
    }
    // Перевод координат виджета в координаты на изображении карты
    int mapX = (widgetPos.x() * mapVisibleArea.width() / width()) + mapVisibleArea.left();
    int mapY = (widgetPos.y() * mapVisibleArea.height() / height()) + mapVisibleArea.top();

    return QPoint(mapX, mapY);
}

QPoint MapWidget::mapToWidgetPos(QPoint mapPos)
{
    if (mapVisibleArea.isNull() || mapVisibleArea.isEmpty())
    {
        return QPoint (0, 0);
    }
    // Перевод координат на изображении карты в координаты виджета
    int widgetX = (mapPos.x() - mapVisibleArea.left()) * width() / mapVisibleArea.width();
    int widgetY = (mapPos.y() - mapVisibleArea.top()) * height() / mapVisibleArea.height();

    return QPoint(widgetX, widgetY);
}
