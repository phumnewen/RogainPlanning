RogainPlanning
--------------

This is a GUI application that allows you quickly and easily plan you route on rogain competition.

How To Use
----------

1. Set the time limit
2. Set your approximate average speed at the distance
3. Then your path limit will be calculated
4. Load map image (if map does not contain grafical scale representation, use a ruler as a scale while photoing the map)
5. Set the scale: 
6. Arrange control points by the map. If you click on the map widget in point addition mode the point parameters dialog apears,
   where it will be offered to you to enter name and score of the point.
   Usually points are named by double-digit number, where first digit is a score. So if you enter point name as a number score 
   will be filled automatically.
7. Lead your route line. If you set a route point into circle of control point the first will be transfered to control point and 
   your total route scores will be increased automatically. Also while you are leading the route the path length recalculates automatically too.