#ifndef MAPWIDGET_H
#define MAPWIDGET_H

#include <cmath>
#include <QObject>
#include <QWidget>
#include <QPainter>
#include <QImage>
#include <QRect>
#include <QPoint>
#include <QDebug>
#include <QResizeEvent>
#include <QMessageBox>

class MapWidget : public QWidget
{
    Q_OBJECT
public:

    explicit MapWidget(QWidget *parent = nullptr);

    enum MapMode
    {
        surfing = 0,
        scaleSetting = 1,
        cpAddition = 2,
        routeLeading = 3
    };


signals:

    void scaleSet(QLine scaleLine);

    void CPAdded(QPoint CP);
    void routePointAdded(QPoint routePoint, int cpMatchedIndex);

    void CPremoved(int CPIdx);
    void routePointRemoved(int routePointIdx);


public slots:

    void loadImage (QString imageFileName);
    void setMapMode (MapWidget::MapMode mode);
    void removeLastPoint();


private:

    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);
    void enterEvent(QEnterEvent *event);
    void leaveEvent(QEvent *event);

    QPoint widgetToMapPos(QPoint widgetPos);
    QPoint mapToWidgetPos(QPoint mapPos);


    QImage map;
    QRect mapVisibleArea;
    QLine scaleLine;
    bool scaleIsSet = false;
    QVector <QPoint> CPs;
    QVector <QPoint> routePoints;
    int pointCircleDiam;
    int pointDotDiam;
    MapWidget::MapMode mapMode;
    QPoint dragStartPoint;


};

#endif // MAPWIDGET_H
