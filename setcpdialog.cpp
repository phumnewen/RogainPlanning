#include "setcpdialog.h"
#include "ui_setcpdialog.h"

SetCPDialog::SetCPDialog(QString *name, int *score, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SetCPDialog),
    _name(name), _score(score)
{
    ui->setupUi(this);
}

SetCPDialog::~SetCPDialog()
{
    delete ui;
}

void SetCPDialog::accept()
{
    *_name = ui->lineEdit_poointName->text();
    *_score = ui->spinBox_pointScore->value();
    QDialog::accept();
}

void SetCPDialog::on_lineEdit_poointName_textChanged(const QString &arg1)
{
    if (!arg1.isEmpty())
    {
        bool ok;
        int score = QString(arg1.at(0)).toInt(&ok);
        if (ok)
        {
            ui->spinBox_pointScore->setValue(score);
        }
    }
}
