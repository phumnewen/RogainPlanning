#ifndef SETCPDIALOG_H
#define SETCPDIALOG_H

#include <QDialog>

namespace Ui {
class SetCPDialog;
}

class SetCPDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SetCPDialog(QString *name, int *score, QWidget *parent = 0);
    ~SetCPDialog();


signals:

private slots:

    void accept();

    void on_lineEdit_poointName_textChanged(const QString &arg1);

private:

    Ui::SetCPDialog *ui;

    QString *_name;
    int *_score;
};

#endif // SETCPDIALOG_H
