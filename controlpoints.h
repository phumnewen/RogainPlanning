#ifndef CONTROLPOINTS_H
#define CONTROLPOINTS_H

#include <QObject>
#include <QVector>
#include <QPoint>

class ControlPoints : public QObject
{
    Q_OBJECT
public:
    explicit ControlPoints(QObject *parent = nullptr);

    QString getCPName (int CPIndex);
    int getCPScore (int CPIndex);

signals:

    void controlPointsChanged (QVector <QString> cpNames,
                               QVector <int> cpScores,
                               QVector <QPointF> cpPositions);


public slots:
    void setMapScale(double scale);

    void addCP (QString name, int score, QPoint position);
    void removeCP (int CPIndex);
    void renameCP (int CPIndex, QString newName);
    void setCPScore (int CPIndex, int newScore);
    void changeCPPosition (int CPIndex, QPoint newPosition);


private:

    QVector <QString> cpNames;
    QVector <int> cpScores;
    QVector <QPointF> cpPositions;
    double kmPerPx = 0.0;


};

#endif // CONTROLPOINTS_H
